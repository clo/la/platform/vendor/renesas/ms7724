LOCAL_PATH := $(call my-dir)


####################################################################

# Lets install our own init.rc files :)
# We will also make the ramdisk depend on it so that it's always pulled in.

LOCAL_PATH := vendor/renesas/ms7724
include $(CLEAR_VARS)

target_init_rc_file := $(TARGET_ROOT_OUT)/init.rc
$(target_init_rc_file) : $(LOCAL_PATH)/init.rc | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(target_init_rc_file)

target_hw_init_rc_file := $(TARGET_ROOT_OUT)/init.r0p7724.rc
$(target_hw_init_rc_file) : $(LOCAL_PATH)/init.r0p7724.rc | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(target_hw_init_rc_file)

$(INSTALLED_RAMDISK_TARGET): $(target_init_rc_file) $(target_hw_init_rc_file)

# and our initialization script
file := $(TARGET_OUT)/etc/init.r0p7724.sh
$(file) : $(LOCAL_PATH)/init.r0p7724.sh | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)


####################################################################
### Include input devices specific files

include $(CLEAR_VARS)

file := $(TARGET_OUT_KEYLAYOUT)/sh_keysc.kl
ALL_PREBUILT += $(file)
$(file) : $(LOCAL_PATH)/sh_keysc.kl | $(ACP)
	$(transform-prebuilt-to-target)


####################################################################
### Copy necessary files to target root filesystem

PRODUCT_COPY_FILES += \
	vendor/renesas/ms7724/vold.conf:system/etc/vold.conf \
	vendor/renesas/ms7724/asound.conf:system/etc/asound.conf


# Pick up some sounds
include frameworks/base/data/sounds/OriginalAudio.mk
